import { Component, OnInit } from '@angular/core';
declare const $: any;

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    initQuantity();
    function initQuantity() {
      if ($(".plus").length && $(".minus").length) {
        var plus = $(".plus");
        var minus = $(".minus");
        var value = $("#quantity_value");

        plus.on("click", function() {
          var x = parseInt(value.text());
          value.text(x + 1);
        });

        minus.on("click", function() {
          var x = parseInt(value.text());
          if (x > 1) {
            value.text(x - 1);
          }
        });
      }
    }
  }

}
