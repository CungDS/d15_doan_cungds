import { Component, OnInit } from '@angular/core';
declare const myTest: any;
declare const $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  styles: [`
        @media only screen and(max-width: 991px) {
          #hello{
              font-size: 30px;
              color: blue;
          }
      }
    `]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('li').click(function() {
      $(this)
        .addClass('active')
        .siblings()
        .removeClass('active');
    });

    $('#brand_carouse').owlCarousel({
      items: 6,
      navigation: true,
      pagination: false
    });

    initFavorite();
    initFixProductBorder();
    initIsotopeFiltering();

    function initFavorite() {
      if ($('.favorite').length) {
        const favs = $('.favorite');

        favs.each(function() {
          const fav = $(this);
          let active = false;
          if (fav.hasClass('active')) {
            active = true;
          }

          fav.on('click', () => {
            if (active) {
              fav.removeClass('active');
              active = false;
            } else {
              fav.addClass('active');
              active = true;
            }
          });
        });
      }
    }

    function initFixProductBorder() {
      if ($('.product_filter').length) {
        const products = $('.product_filter:visible');
        const wdth = window.innerWidth;

        // reset border
        products.each(function() {
          $(this).css('border-right', 'solid 1px #e9e9e9');
        });

        // if window width is 991px or less

        if (wdth < 480) {
          for (let i = 0; i < products.length; i++) {
            let product = $(products[i]);
            product.css('border-right', 'none');
          }
        } else if (wdth < 576) {
          if (products.length < 5) {
            let product = $(products[products.length - 1]);
            product.css('border-right', 'none');
          }
          for (let i = 1; i < products.length; i += 2) {
            let product = $(products[i]);
            product.css('border-right', 'none');
          }
        } else if (wdth < 768) {
          if (products.length < 5) {
            let product = $(products[products.length - 1]);
            product.css('border-right', 'none');
          }
          for (let i = 2; i < products.length; i += 3) {
            let product = $(products[i]);
            product.css('border-right', 'none');
          }
        } else if (wdth < 992) {
          if (products.length < 5) {
            let product = $(products[products.length - 1]);
            product.css('border-right', 'none');
          }
          for (let i = 3; i < products.length; i += 4) {
            let product = $(products[i]);
            product.css('border-right', 'none');
          }
        } else {
          if (products.length < 5) {
            let product = $(products[products.length - 1]);
            product.css('border-right', 'none');
          }
          for (let i = 4; i < products.length; i += 5) {
            let product = $(products[i]);
            product.css('border-right', 'none');
          }
        }
      }
    }

    function initIsotopeFiltering() {
      if ($('.grid_sorting_button').length) {
        $('.grid_sorting_button').click(function() {
          // putting border fix inside of setTimeout because of the transition duration
          setTimeout(function() {
            initFixProductBorder();
          }, 500);

          $('.grid_sorting_button.active').removeClass('active');
          $(this).addClass('active');

          let selector = $(this).attr('data-filter');
          $('.product-grid').isotope({
            filter: selector,
            animationOptions: {
              duration: 750,
              easing: 'linear',
              queue: false
            }
          });

          return false;
        });
      }
    }
  }

}
