import { Component, OnInit } from '@angular/core';
declare const $: any;

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    initThumbnail();
    initQuantity();
    initStarRating();
    initFavorite();
    initTabs();

    function initThumbnail() {
      if ($('.single_product_thumbnails ul li').length) {
        var thumbs = $('.single_product_thumbnails ul li');
        var singleImage = $('.single_product_image_background');

        thumbs.each(function() {
          var item = $(this);
          item.on('click', function() {
            thumbs.removeClass('active');
            item.addClass('active');
            var img = item.find('img').data('image');
            singleImage.css('background-image', 'url(' + img + ')');
          });
        });
      }
    }

    function initQuantity() {
      if ($('.plus').length && $('.minus').length) {
        var plus = $('.plus');
        var minus = $('.minus');
        var value = $('#quantity_value');

        plus.on('click', function() {
          var x = parseInt(value.text());
          value.text(x + 1);
        });

        minus.on('click', function() {
          var x = parseInt(value.text());
          if (x > 1) {
            value.text(x - 1);
          }
        });
      }
    }

    function initStarRating() {
      if ($('.user_star_rating li').length) {
        let stars = $('.user_star_rating li');

        stars.each(function() {
          let star = $(this);

          star.on('click', () => {
            let i = star.index();

            stars.find('i').each(function() {
              $(this).removeClass('fa-star');
              $(this).addClass('fa-star-o');
            });
            for (let x = 0; x <= i; x++) {
              $(stars[x])
                .find('i')
                .removeClass('fa-star-o');
              $(stars[x])
                .find('i')
                .addClass('fa-star');
            }
          });
        });
      }
    }

    function initFavorite() {
      if ($('.product_favorite').length) {
        var fav = $('.product_favorite');

        fav.on('click', function() {
          fav.toggleClass('active');
        });
      }
    }

    function initTabs() {
      if ($('.tabs').length) {
        var tabs = $('.tabs li');
        var tabContainers = $('.tab_container');

        tabs.each(function() {
          var tab = $(this);
          var tab_id = tab.data('active-tab');

          tab.on('click', function() {
            if (!tab.hasClass('active')) {
              tabs.removeClass('active');
              tabContainers.removeClass('active');
              tab.addClass('active');
              $('#' + tab_id).addClass('active');
            }
          });
        });
      }
    }
  }

}
