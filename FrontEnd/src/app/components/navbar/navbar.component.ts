import { Component, OnInit } from '@angular/core';
declare const $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    const header = $('.header');
    const hamburger = $('.hamburger_container');
    const menu = $('.hamburger_menu');
    let menuActive = false;
    const hamburgerClose = $('.hamburger_close');
    const fsOverlay = $('.fs_menu_overlay');

    setHeader();
    initMenu();

    $('.js-show-cart').on('click', () => {
      $('.js-panel-cart').addClass('show-header-cart');
    });

    $('.js-hide-cart').on('click', () => {
      $('.js-panel-cart').removeClass('show-header-cart');
    });

    $('.js-show-modal-search').on('click', () => {
      $('.modal-search-header').addClass('show-modal-search');
      $(this).css('opacity', '0');
    });

    $('.js-hide-modal-search').on('click', () => {
      $('.modal-search-header').removeClass('show-modal-search');
      $('.js-show-modal-search').css('opacity', '1');
    });

    $('.container-search-header').on('click', (e) => {
      e.stopPropagation();
    });
    
    $(window).on('resize', () => {
      // initFixProductBorder();
      setHeader();
    });

    $(document).on('scroll', () => {
      setHeader();
    });

    function setHeader() {
      if (window.innerWidth < 992) {
        if ($(window).scrollTop() > 100) {
          header.css({ top: '0' });
        } else {
          header.css({ top: '0' });
        }
      } else {
        if ($(window).scrollTop() > 100) {
          header.css({ top: '-50px' });
        } else {
          header.css({ top: '0' });
        }
      }
      if (window.innerWidth > 991 && menuActive) {
        closeMenu();
      }
    }

    function initMenu() {
      if (hamburger.length) {
        hamburger.on('click', () => {
          if (!menuActive) {
            openMenu();
          }
        });
      }

      if (fsOverlay.length) {
        fsOverlay.on('click', () => {
          if (menuActive) {
            closeMenu();
          }
        });
      }

      if (hamburgerClose.length) {
        hamburgerClose.on('click', () => {
          if (menuActive) {
            closeMenu();
          }
        });
      }

      if ($('.menu_item').length) {
        const items = document.getElementsByClassName('menu_item') ;
        let i;

        for (i = 0; i < items.length; i++) {
          if (items[i].classList.contains('has-children')) {
            // items[i].click = () => {
            //   this.classList.toggle('active');
            //   let panel = this.children[1];
            //   if (panel.style.maxHeight) {
            //     panel.style.maxHeight = null;
            //   } else {
            //     panel.style.maxHeight = panel.scrollHeight + 'px';
            //   }
            // };
          }
        }
      }
    }

    function openMenu() {
      menu.addClass('active');
      // menu.css('right', "0");
      fsOverlay.css('pointer-events', 'auto');
      menuActive = true;
    }

    function closeMenu() {
      menu.removeClass('active');
      fsOverlay.css('pointer-events', 'none');
      menuActive = false;
    }

   

  }

}
