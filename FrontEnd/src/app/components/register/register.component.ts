import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../helpers/must-match.validation';

declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  showPass = false;
  showEye = false;
  showConfirmPass = false;
  showEyeConfirm = false;

  patternPassword = '^(?=.*[a-z])(?=.*[A-Z])(?=.*)(?=.*[#$^+=!*()@%&]).{8,15}$';
  phonePattern = '^((\\+91-?)|0)?[0-9]{10}$';

  constructor( private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(this.patternPassword)]],
      confirmPassword: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      dob: ['', [Validators.required]],
      address: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.pattern(this.phonePattern)]]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });

    $(() => {
      $('.datepicker').datepicker({
        format: 'yyyy/mm/dd'
      });
    });

  }

  get f() { return this.registerForm.controls; }

  showPassword() {
      this.showPass = !this.showPass;
      this.showEye = !this.showEye;
  }

   showConfirmPassword() {
      this.showConfirmPass = !this.showConfirmPass;
      this.showEyeConfirm = !this.showEyeConfirm;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    alert(this.registerForm.get('userName').value);
  }
}
