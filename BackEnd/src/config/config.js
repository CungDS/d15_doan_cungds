const PORT = 9999;
const SECRET_KEY = '5rbB4SsOCauVmjkWFaP6';
const TOKEN_EXPIRATION_TIME = 86400;

module.exports = {
    PORT,
    SECRET_KEY,
    TOKEN_EXPIRATION_TIME
}

