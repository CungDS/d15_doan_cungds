const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const Config = require('../config/config');
const { responseData, responseToken } = require('../common/response');
const User = require('../model/user');


const fillAll = async (req, res, next) => {
    try {
        const users = await User.find({});
        res.status(200).json(responseData(200, 'List users', '', users));
    } catch (error) {
        next(error);
    }
}

const register = async (req, res, next) => {
    try {
        const email = req.body.email;
        let emailExists = await User.findOne({ email: email });
        if (emailExists) return res.status(400).json(responseData(400, 'Email already exists', '', ''));

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt);

        let newUser = new User({
            email: email,
            password: hashedPassword,
            userName: req.body.userName,
            gender: req.body.gender,
            dob: req.body.dob,
            address: req.body.address,
            phone: req.body.phone
        });

        newUser.save().then(user => {
            res.status(200).json(responseData(200, 'Register Successfully', '', user));
        }).catch(err => {
            res.status(500).json(responseData(500, 'Register Failed', err, ''));
        });
    } catch (error) {
        next(error);
    }
}

const login = async (req, res, next) => {
    try {
        const email = req.body.email;
        let isUser = await User.findOne({ email: email });
        if (!isUser) return res.status(400).json(responseData(400, 'Email is not found', '', ''));

        const validPassword = await bcrypt.compare(req.body.password, isUser.password);
        if (!validPassword) return res.status(400).json(responseData(400, 'Invalid password', '', ''));

        const token = jwt.sign(isUser.toJSON(), Config.SECRET_KEY, {
            expiresIn: Config.TOKEN_EXPIRATION_TIME
        });

        // res.status(200).json(responseToken(200, 'Login successfully', token, '', isUser));
        res.status(200).json(responseToken(200, 'Login successfully', token, '', ''));
    } catch (error) {
        next(error);
    }
}

const profile = async (req, res, next) => {
    try {
        res.status(200).json(responseData(200, 'Profile user', '', req.user))
    } catch (error) {
        next(error);
    }
}

module.exports = {
    fillAll,
    register,
    login,
    profile
}