const Config = require('../config/config');
const { responseData } = require('../common/response');
const Cart = require('../model/cart');
const common = require('../common/function');

const fillAll = async (req, res, next) => {
    try {
        const cart = await Cart.find({});
        res.status(200).json(responseData(200, 'List Cart', '', cart));
    } catch (error) {
        next(error);
    }
}

const fillCartByID = async (req, res, next) => {
    try {
        let cartID = req.params.ID;
        Cart.findOne({ ID: cartID }, (err, cart) => {
            (cart) ? res.status(200).json(responseData(200, 'Find Cart success', '', cart)) : res.json(responseData(200, 'Cart not exists', '', ''))
        });
    } catch (error) {
        next(error);
    }
}

const createCart = async (req, res, next) => {
    try {
        let { body } = req;
        body['ID'] = common.randomCartID();
        let cart = new Cart(body);
        cart.save().then(cart => {
            res.status(200).json(responseData(200, 'Create new Cart success', '', cart))
        }).catch(err => {
            res.send('Failed to create new records');
        });
    } catch (error) {
        next(error);
    }
}

const updateCartByID = async (req, res, next) => {
    try {
        let cartID = req.params.ID;
        let oddCart = await Cart.findOne({ ID: cartID });
        let newCart = req.body;
        newCart['_id'] = oddCart._id;
        newCart['ID'] = oddCart.ID;
        let cart = new Cart(newCart);
        Cart.findOneAndUpdate({ ID: cartID }, { $set: cart }, { upsert: true }, (err, cart) => {
            (!cart) ? res.status(200).json(responseData(500, "Update Cart failed", err, "")) : res.json(responseData(200, "Update Cart Successfully", "", cart));
        });
    } catch (error) {
        next(error);
    }
}

const deleteCartByID = async (req, res, next) => {
    try {
        let cartID = req.params.ID;
        Cart.findOneAndRemove({ ID: cartID }, (err, cart) => {
            (err || !cart) ? res.status(200).json(responseData(500, 'Cart not found', 'Delete failed', '')) : res.json(responseData(200, 'Cart has been deleted', '', cart.ID));
        });
    } catch (error) {
        next(error);
    }
}

module.exports = {
    fillAll,
    fillCartByID,
    createCart,
    updateCartByID,
    deleteCartByID
}