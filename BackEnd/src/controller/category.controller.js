const Config = require('../config/config');
const { responseData } = require('../common/response');
const Category = require('../model/category');
const common = require('../common/function');

const fillAll = async (req, res, next) => {
    try {
        const category = await Category.find({});
        res.status(200).json(responseData(200, 'List Category', '', category));
    } catch (error) {
        next(error);
    }
}

const fillCategoryByID = async (req, res, next) => {
    try {
        let categoryID = req.params.ID;
        Category.findOne({ ID: categoryID }, (err, category) => {
            (Category) ? res.status(200).json(responseData(200, 'Find Category success', '', category)) : res.json(responseData(200, 'Category not exists', '', ''))
        });
    } catch (error) {
        next(error);
    }
}

const createCategory = async (req, res, next) => {
    try {
        let { body } = req;
        body['ID'] = common.randomCategoryID();
        let category = new Category(body);
        category.save().then(category => {
            res.status(200).json(responseData(200, 'Create new Category success', '', category))
        }).catch(err => {
            res.send('Failed to create new records');
        });
    } catch (error) {
        next(error);
    }
}

const updateCategoryByID = async (req, res, next) => {
    try {
        let categoryID = req.params.ID;
        let oddCategory = await Category.findOne({ ID: categoryID });
        let newCategory = req.body;
        newCategory['_id'] = oddCategory._id;
        newCategory['ID'] = oddCategory.ID;
        let category = new Category(newCategory);
        Category.findOneAndUpdate({ ID: categoryID }, { $set: category }, { upsert: true }, (err, cate) => {
            (!cate) ? res.status(200).json(responseData(500, "Update Category failed", err, "")) : res.json(responseData(200, "Update Category Successfully", "", category));
        });
    } catch (error) {
        next(error);
    }
}

const deleteCategoryByID = async (req, res, next) => {
    try {
        let categoryID = req.params.ID;
        Category.findOneAndRemove({ ID: categoryID }, (err, category) => {
            (err || !category) ? res.status(200).json(responseData(500, 'Category not found', 'Delete failed', '')) : res.json(responseData(200, 'Category has been deleted', '', category.ID));
        });
    } catch (error) {
        next(error);
    }
}

module.exports = {
    fillAll,
    fillCategoryByID,
    createCategory,
    updateCategoryByID,
    deleteCategoryByID
}