const Config = require('../config/config');
const { responseData } = require('../common/response');
const Order = require('../model/order');
const common = require('../common/function');

const fillAll = async (req, res, next) => {
    try {
        const order = await Order.find({});
        res.status(200).json(responseData(200, 'List Order', '', order));
    } catch (error) {
        next(error);
    }
}

const fillOrderByID = async (req, res, next) => {
    try {
        let OrderID = req.params.ID;
        Order.findOne({ ID: OrderID }, (err, order) => {
            (order) ? res.status(200).json(responseData(200, 'Find Order success', '', order)) : res.json(responseData(200, 'Order not exists', '', ''))
        });
    } catch (error) {
        next(error);
    }
}

const createOrder = async (req, res, next) => {
    try {
        let { body } = req;
        body['ID'] = common.randomOrderID();
        let order = new Order(body);
        order.save().then(order => {
            res.status(200).json(responseData(200, 'Create new Order success', '', order))
        }).catch(err => {
            res.send('Failed to create new records');
        });
    } catch (error) {
        next(error);
    }
}

const updateOrderByID = async (req, res, next) => {
    try {
        let orderID = req.params.ID;
        let oddOrder = await Order.findOne({ ID: orderID });
        let newOrder = req.body;
        newOrder['_id'] = oddOrder._id;
        newOrder['ID'] = oddOrder.ID;
        let order = new Order(newOrder);
        Order.findOneAndUpdate({ ID: orderID }, { $set: order }, { upsert: true }, (err, ord) => {
            (!ord) ? res.status(200).json(responseData(500, "Update Order failed", err, "")) : res.json(responseData(200, "Update Order Successfully", "", ord));
        });
    } catch (error) {
        next(error);
    }
}

const deleteOrderByID = async (req, res, next) => {
    try {
        let orderID = req.params.ID;
        Order.findOneAndRemove({ ID: orderID }, (err, order) => {
            (err || !order) ? res.status(200).json(responseData(500, 'Order not found', 'Delete failed', '')) : res.json(responseData(200, 'Order has been deleted', '', order.ID));
        });
    } catch (error) {
        next(error);
    }
}

module.exports = {
    fillAll,
    fillOrderByID,
    createOrder,
    updateOrderByID,
    deleteOrderByID
}