const Config = require('../config/config');
const { responseData } = require('../common/response');
const Bill = require('../model/bill');
const common = require('../common/function');

const fillAll = async (req, res, next) => {
    try {
        const bill = await Bill.find({});
        res.status(200).json(responseData(200, 'List Bill', '', bill));
    } catch (error) {
        next(error);
    }
}

const fillByBillID = async (req, res, next) => {
    try {
        let billID = req.params.ID;
        Bill.findOne({ ID: billID }, (err, bill) => {
            (bill) ? res.status(200).json(responseData(200, 'Find Bill success', '', bill)) : res.json(responseData(200, 'Bill not exists', '', ''))
        });
    } catch (error) {
        next(error);
    }
}

const createBill = async (req, res, next) => {
    try {
        let { body } = req;
        body['ID'] = common.randomBillID();
        let bill = new Bill(body);
        bill.save().then(bill => {
            res.status(200).json(responseData(200, 'Create new Bill success', '', bill))
        }).catch(err => {
            res.send('Failed to create new records');
        });
    } catch (error) {
        next(error);
    }
}

const updateBillByID = async (req, res, next) => {
    try {
        let billID = req.params.ID;
        let oddBill = await Bill.findOne({ ID: billID });
        let newBill = req.body;
        newBill['_id'] = oddBill._id;
        newBill['ID'] = oddBill.ID;
        let bill = new Bill(newBill);
        Bill.findOneAndUpdate({ ID: billID }, { $set: bill }, { upsert: true }, (err, bill) => {
            (!bill) ? res.status(200).json(responseData(500, "Update Bill failed", err, "")) : res.json(responseData(200, "Update Bill Successfully", "", bill));
        });
    } catch (error) {
        next(error);
    }
}

const deleteBillByID = async (req, res, next) => {
    try {
        let billID = req.params.ID;
        Bill.findOneAndRemove({ ID: billID }, (err, bill) => {
            (err || !bill) ? res.status(200).json(responseData(500, 'Bill not found', 'Delete failed', '')) : res.json(responseData(200, 'Bill has been deleted', '', bill.ID));
        });
    } catch (error) {
        next(error);
    }
}

module.exports = {
    fillAll,
    fillByBillID,
    createBill,
    updateBillByID,
    deleteBillByID
}