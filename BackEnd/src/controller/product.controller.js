const Config = require('../config/config');
const { responseData } = require('../common/response');
const Product = require('../model/product');
const common = require('../common/function');

const fillAll = async (req, res, next) => {
    try {
        const products = await Product.find({});
        res.status(200).json(responseData(200, 'List product', '', products));
    } catch (error) {
        next(error);
    }
}

const fillProductByID = async (req, res, next) => {
    try {
        let productID = req.params.ID;
        Product.findOne({ ID: productID }, (err, product) => {
            (product) ? res.status(200).json(responseData(200, 'Find product success', '', product)) : res.json(responseData(200, 'Product not exists', '', ''))
        });
    } catch (error) {
        next(error);
    }
}

const createProduct = async (req, res, next) => {
    try {
        let {body} = req;
        body['ID'] = common.randomProductID();
        let product = new Product(body);
        product.save().then(product => {
            res.status(200).json(responseData(200, 'Create new product success', '', product))
        }).catch(err => {
            res.send('Failed to create new records');
        });
    } catch (error) {
        next(error);
    }
}

const updateProductByID = async (req, res, next) => {
    try {
        let productID = req.params.ID;
        let oddProduct = await Product.findOne({ ID: productID });
        let newProduct = req.body;
        newProduct['_id'] = oddProduct._id;
        newProduct['ID'] = oddProduct.ID;
        let product = new Product(newProduct);
        Product.findOneAndUpdate({ ID: productID }, { $set: product }, { upsert: true }, (err, pro) => {
            (!pro) ? res.status(200).json(responseData(500, "Update Product failed", err, "")) : res.json(responseData(200, "Update Product Successfully", "", product));
        });
    } catch (error) {
        next(error);
    }
}

const deleteProductByID = async (req, res, next) => {
    try {
        let productID = req.params.ID;
        Product.findOneAndRemove({ ID: productID }, (err, product) => {
            (err || !product) ? res.status(200).json(responseData(500, 'Product not found', 'Delete failed', '')) : res.json(responseData(200, 'Product has been deleted', '', product.ID));
        });
    } catch (error) {
        next(error);
    }
}

module.exports = {
    fillAll,
    fillProductByID,
    createProduct,
    updateProductByID,
    deleteProductByID
}