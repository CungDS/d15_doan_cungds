const passport = require('passport');
require('../common/passport')(passport);

module.exports = function (app) {
    const bill = require('../controller/bill.controller');
    const cart = require('../controller/cart.controller');
    const category = require('../controller/category.controller');
    const order = require('../controller/order.controller');
    const product = require('../controller/product.controller');
    const users = require('../controller/user.controller');

    /* Bill */
    app.get('/api/v1/bills', bill.fillAll);

    app.get('/api/v1/bill/:ID', bill.fillByBillID);

    app.post('/api/v1/createBill', bill.createBill);

    app.put('/api/v1/updateBill/:ID', bill.updateBillByID);

    app.delete('/api/v1/deleteBill/:ID', bill.deleteBillByID);

    /* Cart */
    app.get('/api/v1/carts', cart.fillAll);

    app.get('/api/v1/cart/:ID', cart.fillCartByID);

    app.post('/api/v1/createCart', cart.createCart);

    app.put('/api/v1/updateCart/:ID', cart.updateCartByID);

    app.delete('/api/v1/deleteCart/:ID', cart.deleteCartByID);

    /* Category */
    app.get('/api/v1/categories', category.fillAll);

    app.get('/api/v1/category/:ID', category.fillCategoryByID);

    app.post('/api/v1/createCategory', category.createCategory);

    app.put('/api/v1/updateCategory/:ID', category.updateCategoryByID);

    app.delete('/api/v1/deleteCategory/:ID', category.deleteCategoryByID);

    /* Order */
    app.get('/api/v1/orders', order.fillAll);

    app.get('/api/v1/order/:ID', order.fillOrderByID);

    app.post('/api/v1/createOrder', order.createOrder);

    app.put('/api/v1/updateOrder/:ID', order.updateOrderByID);

    app.delete('/api/v1/deleteOrder/:ID', order.deleteOrderByID);

    /* Product */
    app.get('/api/v1/products', product.fillAll);

    app.get('/api/v1/product/:ID', product.fillProductByID);

    app.post('/api/v1/createProduct', product.createProduct);

    app.put('/api/v1/updateProduct/:ID', product.updateProductByID);

    app.delete('/api/v1/deleteProduct/:ID', product.deleteProductByID);

    /* Users */
    app.get('/api/v1/users', users.fillAll);

    app.post('/api/v1/register', users.register);

    app.post('/api/v1/login', users.login);

    app.get('/api/v1/profile', passport.authenticate('jwt', { session: false }), users.profile);

}