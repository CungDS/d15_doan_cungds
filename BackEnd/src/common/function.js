const crypto = require("crypto");

const randomNumber = () => {
    return crypto.randomBytes(5).toString('hex');
}

const randomCategoryID = () => {
    return "CA" + crypto.randomBytes(5).toString('hex');
}

const randomProductID = () => {
    return "PR"+crypto.randomBytes(5).toString('hex');
}

const randomCartID = () => {
    return "CA" + crypto.randomBytes(5).toString('hex');
}

const randomBillID = () => {
    return "BI" + crypto.randomBytes(5).toString('hex');
}

const randomOrderID = () => {
    return "OR" + crypto.randomBytes(5).toString('hex');
}

module.exports = {
    randomNumber,
    randomCategoryID,
    randomProductID,
    randomCartID,
    randomBillID,
    randomOrderID
}