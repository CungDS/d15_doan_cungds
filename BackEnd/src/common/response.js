const responseData = (status, messenger, error, data) => {
    return response = {
        'status': status,
        'messenger': messenger,
        'error': error,
        'data': data
    }
}

const responseToken = (status, messenger, token, error, data) => {
    return response = {
        'status': status,
        'messenger': messenger,
        'token': 'JWT '+ token,
        'error': error,
        'data': data
    }
}

module.exports = {
    responseData,
    responseToken
}