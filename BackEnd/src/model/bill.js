const mongoose = require('mongoose');

const Bill = new mongoose.Schema({
    ID: { type: String, unique: true },
    // order: { type: mongoose.Types.ObjectId, ref: 'Order' },
    cart: [{
        productID: {type: String},
        quantity: {type: String},
    }],
    orderID: {type: String},
    totalPrice: { type: String },
    payment: {type: String},
    createAt: { type: Date, default: new Date() }
});

module.exports = mongoose.model('Bill', Bill);