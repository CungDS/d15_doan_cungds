const mongoose = require('mongoose');

const Category = new mongoose.Schema({
    ID: { type: String, unique: true },
    name: { type: String },
    subCategory: [{
        type: {type: String},
        title: {type: String},
        gender: {type: String}
    }],
    type: { type: String },
    createAt: { type: Date, default: new Date() }
});

module.exports = mongoose.model('Category', Category);