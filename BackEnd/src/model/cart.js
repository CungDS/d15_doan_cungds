const mongoose = require('mongoose');

const Cart = new mongoose.Schema({
    ID: { type: String, unique: true },
    listItem: [{
        // productID: { type: mongoose.Types.ObjectId, ref: 'Product' },
        productID: {type: String},
        quantity: {type: String },
        price: {type: String},
        color: {type: String},
        size: {type: String}
    }],
    totalPrice: {type: String},
    createAt: { type: Date, default: new Date() }
});

module.exports = mongoose.model('Cart', Cart);