const mongoose = require('mongoose');

const Order = new mongoose.Schema({
    ID: { type: String, unique: true },
    // cartID: { type: mongoose.Types.ObjectId, ref: 'Cart' },
    status: { type: String },
    ship: {
        address: {type: String},
        phone: {type: String},
        email: {type: String},
    },
    cartID: {type: String},
    totalPrice: { type: String },
    dateOrder: { type: Date, default: new Date() },
    createAt: { type: Date, default: new Date() }
});

module.exports = mongoose.model('Order', Order);