const mongoose = require('mongoose');

const User = new mongoose.Schema({
    email: { type: String, unique: true },
    password: { type: String },
    userName: { type: String },
    gender: { type: String },
    dob: { type: Date },
    address: { type: String },
    phone: { type: String },
    createAt: {type: Date, default: new Date()}
});

module.exports = mongoose.model('User', User);