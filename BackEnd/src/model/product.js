const mongoose = require('mongoose');

const Product = new mongoose.Schema({
    ID: { type: String, unique: true },
    // categoryID: { type: mongoose.Types.ObjectId, ref: 'Category' },
    categoryID: {type: String },
    name: { type: String },
    img: { type: String },
    coverPrice: { type: String },
    originPrice: { type: String },
    description: { type: String },
    rate: { type: String },
    brand: { type: String },
    detail: {
        imgDetail1: { type: String },
        imgDetail2: { type: String },
        imgDetail3: { type: String },
        descriptionDetail: { type: String },
    },
    size: { type: String },
    color: { type: String },
    type: { type: String },
    createAt: { type: Date, default: new Date() }
});

module.exports = mongoose.model('Product', Product);