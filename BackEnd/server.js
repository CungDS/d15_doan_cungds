const mongoose = require('mongoose');
const server = require('./src/app');
const {PORT} = require('./src/config/config');

const mongoDB = 'mongodb://localhost:27017/DoAn';
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);


server.listen(PORT);